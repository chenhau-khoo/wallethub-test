# wallethub-test

## 2 scripts created to run different results"
1. ./hourly.sh
2. ./daily.sh

## SQL Query
Query can found under com.ef.repo.AccessLogRepo

## SQL Schema
The project is using JPA (hibernate) to do CRUD against database, however, you can still find the schema below:

CREATE TABLE `access_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `request` text,
  `status` int(11) DEFAULT NULL,
  `user_agent` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=116485 DEFAULT CHARSET=latin1