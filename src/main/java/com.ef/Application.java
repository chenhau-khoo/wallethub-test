package com.ef;

import com.ef.service.ParserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Slf4j
@SpringBootApplication
public class Application implements CommandLineRunner {

    @Autowired
    private ParserService parserService;

    private static final DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        String logFile = args[0];
        String start = args[1];
        String duration = args[2];
        Integer threshold = Integer.valueOf(args[3]);
        log.info("Params:");
        for (int i = 0; i < args.length; ++i) {
            log.info("args[{}]: {}", i, args[i]);
        }

        Date startTime = format.parse(start);
        Date endTime = getEndTime(startTime, duration);
        parserService.run(logFile, startTime, endTime, threshold);
    }

    private static Date getEndTime(Date startTime, String duration) {
        Calendar c = Calendar.getInstance();
        c.setTime(startTime);
        Date endTime = null;
        switch (duration) {
            case "hourly" :
                c.add(Calendar.HOUR, 1);
                endTime = c.getTime();
                break;

            case "daily":
                c.add(Calendar.DATE, 1);
                endTime = c.getTime();
                break;
        }
        return endTime;
    }
}
