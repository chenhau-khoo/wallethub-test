package com.ef.model;

import lombok.Data;


public interface AccessLogCount {
    String getIp();

    Long getCount();
}
