package com.ef.repo;

import com.ef.entity.AccessLog;
import com.ef.model.AccessLogCount;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface AccessLogRepo extends CrudRepository<AccessLog, Long> {

    @Query(value = "select ip, count(1) from access_log "
            + "where date between :from and :to "
            + "group by ip "
            + "having count(1) >= :thershold", nativeQuery = true
    )
    List<AccessLogCount> findByDateBetweenGroupByIp(@Param("from") Date from, @Param("to") Date to, @Param("thershold") Integer thershold);

    AccessLog findOneById(Long id);
}
