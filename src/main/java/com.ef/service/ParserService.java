package com.ef.service;

import com.ef.entity.AccessLog;
import com.ef.model.AccessLogCount;
import com.ef.repo.AccessLogRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ParserService {

    //2017-01-01 00:01:42.407
    private static final DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

    @Autowired
    private AccessLogRepo accessLogRepo;

    public void run(String fileName, Date startTime, Date endTime, Integer threshold) throws IOException, ParseException {

        // insert to db if data not exists
        if (accessLogRepo.findOneById(1L) == null) {
            String logFile = getFileFromStream(fileName);
            List<String> lines = new BufferedReader(new StringReader(logFile))
                    .lines()
                    .collect(Collectors.toList());

            List<AccessLog> accessLogs = new ArrayList<>();
            for (String line : lines) {
                AccessLog accessLog = new AccessLog();
                String[] rows = line.split("\\|");
                Date date = format.parse(rows[0]);
                accessLog.setDate(new Timestamp(date.getTime()));
                accessLog.setIp(rows[1]);
                accessLog.setRequest(rows[2]);
                accessLog.setStatus(Integer.parseInt(rows[3]));
                accessLog.setUserAgent(rows[4]);
                accessLogs.add(accessLog);
                accessLogRepo.save(accessLog);
            }
        }

        // find logs
        List<AccessLogCount> accessLogs = accessLogRepo.findByDateBetweenGroupByIp(startTime, endTime, threshold);
        for (AccessLogCount accessLog : accessLogs) {
            log.info(String.format("IP: %s, Count: %d", accessLog.getIp(), accessLog.getCount()));
        }
    }

    private String getFileFromStream(String fileName) throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        InputStream inputStream = classLoader.getResourceAsStream(fileName);
        String data = readFromInputStream(inputStream);
        return data;
    }

    private String readFromInputStream(InputStream inputStream)
            throws IOException {
        StringBuilder resultStringBuilder = new StringBuilder();
        try (BufferedReader br
                     = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            while ((line = br.readLine()) != null) {
                resultStringBuilder.append(line).append("\n");
            }
        }
        return resultStringBuilder.toString();
    }
}
